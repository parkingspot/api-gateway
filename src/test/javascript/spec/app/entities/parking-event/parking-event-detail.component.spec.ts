/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ApigatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ParkingEventDetailComponent } from '../../../../../../main/webapp/app/entities/parking-event/parking-event-detail.component';
import { ParkingEventService } from '../../../../../../main/webapp/app/entities/parking-event/parking-event.service';
import { ParkingEvent } from '../../../../../../main/webapp/app/entities/parking-event/parking-event.model';

describe('Component Tests', () => {

    describe('ParkingEvent Management Detail Component', () => {
        let comp: ParkingEventDetailComponent;
        let fixture: ComponentFixture<ParkingEventDetailComponent>;
        let service: ParkingEventService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ApigatewayTestModule],
                declarations: [ParkingEventDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ParkingEventService,
                    JhiEventManager
                ]
            }).overrideTemplate(ParkingEventDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ParkingEventDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ParkingEventService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ParkingEvent(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.parkingEvent).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
