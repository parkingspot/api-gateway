/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { ApigatewayTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ParkingZoneDetailComponent } from '../../../../../../main/webapp/app/entities/parking-zone/parking-zone-detail.component';
import { ParkingZoneService } from '../../../../../../main/webapp/app/entities/parking-zone/parking-zone.service';
import { ParkingZone } from '../../../../../../main/webapp/app/entities/parking-zone/parking-zone.model';

describe('Component Tests', () => {

    describe('ParkingZone Management Detail Component', () => {
        let comp: ParkingZoneDetailComponent;
        let fixture: ComponentFixture<ParkingZoneDetailComponent>;
        let service: ParkingZoneService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [ApigatewayTestModule],
                declarations: [ParkingZoneDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ParkingZoneService,
                    JhiEventManager
                ]
            }).overrideTemplate(ParkingZoneDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ParkingZoneDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ParkingZoneService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ParkingZone(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.parkingZone).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
