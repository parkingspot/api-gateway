import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ParkingEventComponent } from './parking-event.component';
import { ParkingEventDetailComponent } from './parking-event-detail.component';
import { ParkingEventPopupComponent } from './parking-event-dialog.component';
import { ParkingEventDeletePopupComponent } from './parking-event-delete-dialog.component';

export const parkingEventRoute: Routes = [
    {
        path: 'parking-event',
        component: ParkingEventComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParkingEvents'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'parking-event/:id',
        component: ParkingEventDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParkingEvents'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const parkingEventPopupRoute: Routes = [
    {
        path: 'parking-event-new',
        component: ParkingEventPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParkingEvents'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'parking-event/:id/edit',
        component: ParkingEventPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParkingEvents'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'parking-event/:id/delete',
        component: ParkingEventDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParkingEvents'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
