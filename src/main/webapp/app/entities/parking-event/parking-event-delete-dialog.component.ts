import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ParkingEvent } from './parking-event.model';
import { ParkingEventPopupService } from './parking-event-popup.service';
import { ParkingEventService } from './parking-event.service';

@Component({
    selector: 'jhi-parking-event-delete-dialog',
    templateUrl: './parking-event-delete-dialog.component.html'
})
export class ParkingEventDeleteDialogComponent {

    parkingEvent: ParkingEvent;

    constructor(
        private parkingEventService: ParkingEventService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.parkingEventService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'parkingEventListModification',
                content: 'Deleted an parkingEvent'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-parking-event-delete-popup',
    template: ''
})
export class ParkingEventDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private parkingEventPopupService: ParkingEventPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.parkingEventPopupService
                .open(ParkingEventDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
