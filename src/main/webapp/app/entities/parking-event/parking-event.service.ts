import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { JhiDateUtils } from 'ng-jhipster';

import { ParkingEvent } from './parking-event.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ParkingEventService {

    private resourceUrl = '/parkingapi/api/parking-events';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(parkingEvent: ParkingEvent): Observable<ParkingEvent> {
        const copy = this.convert(parkingEvent);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(parkingEvent: ParkingEvent): Observable<ParkingEvent> {
        const copy = this.convert(parkingEvent);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<ParkingEvent> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ParkingEvent.
     */
    private convertItemFromServer(json: any): ParkingEvent {
        const entity: ParkingEvent = Object.assign(new ParkingEvent(), json);
        entity.start = this.dateUtils
            .convertDateTimeFromServer(json.start);
        entity.finish = this.dateUtils
            .convertDateTimeFromServer(json.finish);
        return entity;
    }

    /**
     * Convert a ParkingEvent to a JSON which can be sent to the server.
     */
    private convert(parkingEvent: ParkingEvent): ParkingEvent {
        const copy: ParkingEvent = Object.assign({}, parkingEvent);

        copy.start = this.dateUtils.toDate(parkingEvent.start);

        copy.finish = this.dateUtils.toDate(parkingEvent.finish);
        return copy;
    }
}
