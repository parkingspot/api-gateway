import { BaseEntity } from './../../shared';

export class ParkingEvent implements BaseEntity {
    constructor(
        public id?: number,
        public start?: any,
        public finish?: any,
        public parkingZoneId?: number,
    ) {
    }
}
