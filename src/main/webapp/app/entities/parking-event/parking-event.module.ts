import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ApigatewaySharedModule } from '../../shared';
import {
    ParkingEventService,
    ParkingEventPopupService,
    ParkingEventComponent,
    ParkingEventDetailComponent,
    ParkingEventDialogComponent,
    ParkingEventPopupComponent,
    ParkingEventDeletePopupComponent,
    ParkingEventDeleteDialogComponent,
    parkingEventRoute,
    parkingEventPopupRoute,
} from './';

const ENTITY_STATES = [
    ...parkingEventRoute,
    ...parkingEventPopupRoute,
];

@NgModule({
    imports: [
        ApigatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ParkingEventComponent,
        ParkingEventDetailComponent,
        ParkingEventDialogComponent,
        ParkingEventDeleteDialogComponent,
        ParkingEventPopupComponent,
        ParkingEventDeletePopupComponent,
    ],
    entryComponents: [
        ParkingEventComponent,
        ParkingEventDialogComponent,
        ParkingEventPopupComponent,
        ParkingEventDeleteDialogComponent,
        ParkingEventDeletePopupComponent,
    ],
    providers: [
        ParkingEventService,
        ParkingEventPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ApigatewayParkingEventModule {}
