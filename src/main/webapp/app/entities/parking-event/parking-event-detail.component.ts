import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ParkingEvent } from './parking-event.model';
import { ParkingEventService } from './parking-event.service';

@Component({
    selector: 'jhi-parking-event-detail',
    templateUrl: './parking-event-detail.component.html'
})
export class ParkingEventDetailComponent implements OnInit, OnDestroy {

    parkingEvent: ParkingEvent;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private parkingEventService: ParkingEventService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInParkingEvents();
    }

    load(id) {
        this.parkingEventService.find(id).subscribe((parkingEvent) => {
            this.parkingEvent = parkingEvent;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInParkingEvents() {
        this.eventSubscriber = this.eventManager.subscribe(
            'parkingEventListModification',
            (response) => this.load(this.parkingEvent.id)
        );
    }
}
