import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { ParkingEvent } from './parking-event.model';
import { ParkingEventService } from './parking-event.service';

@Injectable()
export class ParkingEventPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private parkingEventService: ParkingEventService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.parkingEventService.find(id).subscribe((parkingEvent) => {
                    parkingEvent.start = this.datePipe
                        .transform(parkingEvent.start, 'yyyy-MM-ddTHH:mm:ss');
                    parkingEvent.finish = this.datePipe
                        .transform(parkingEvent.finish, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.parkingEventModalRef(component, parkingEvent);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.parkingEventModalRef(component, new ParkingEvent());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    parkingEventModalRef(component: Component, parkingEvent: ParkingEvent): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.parkingEvent = parkingEvent;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
