import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ParkingEvent } from './parking-event.model';
import { ParkingEventPopupService } from './parking-event-popup.service';
import { ParkingEventService } from './parking-event.service';
import { ParkingZone, ParkingZoneService } from '../parking-zone';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-parking-event-dialog',
    templateUrl: './parking-event-dialog.component.html'
})
export class ParkingEventDialogComponent implements OnInit {

    parkingEvent: ParkingEvent;
    isSaving: boolean;

    parkingzones: ParkingZone[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private parkingEventService: ParkingEventService,
        private parkingZoneService: ParkingZoneService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.parkingZoneService.query()
            .subscribe((res: ResponseWrapper) => { this.parkingzones = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.parkingEvent.id !== undefined) {
            this.subscribeToSaveResponse(
                this.parkingEventService.update(this.parkingEvent));
        } else {
            this.subscribeToSaveResponse(
                this.parkingEventService.create(this.parkingEvent));
        }
    }

    private subscribeToSaveResponse(result: Observable<ParkingEvent>) {
        result.subscribe((res: ParkingEvent) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ParkingEvent) {
        this.eventManager.broadcast({ name: 'parkingEventListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackParkingZoneById(index: number, item: ParkingZone) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-parking-event-popup',
    template: ''
})
export class ParkingEventPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private parkingEventPopupService: ParkingEventPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.parkingEventPopupService
                    .open(ParkingEventDialogComponent as Component, params['id']);
            } else {
                this.parkingEventPopupService
                    .open(ParkingEventDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
