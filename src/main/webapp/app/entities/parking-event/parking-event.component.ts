import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ParkingEvent } from './parking-event.model';
import { ParkingEventService } from './parking-event.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-parking-event',
    templateUrl: './parking-event.component.html'
})
export class ParkingEventComponent implements OnInit, OnDestroy {
parkingEvents: ParkingEvent[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private parkingEventService: ParkingEventService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.parkingEventService.query().subscribe(
            (res: ResponseWrapper) => {
                this.parkingEvents = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInParkingEvents();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ParkingEvent) {
        return item.id;
    }
    registerChangeInParkingEvents() {
        this.eventSubscriber = this.eventManager.subscribe('parkingEventListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
