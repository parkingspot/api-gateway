import { BaseEntity } from './../../shared';

export class CustomerAccount implements BaseEntity {
    constructor(
        public id?: number,
        public userName?: string,
        public balance?: number,
    ) {
    }
}
