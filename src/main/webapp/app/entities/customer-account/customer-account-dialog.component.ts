import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CustomerAccount } from './customer-account.model';
import { CustomerAccountPopupService } from './customer-account-popup.service';
import { CustomerAccountService } from './customer-account.service';

@Component({
    selector: 'jhi-customer-account-dialog',
    templateUrl: './customer-account-dialog.component.html'
})
export class CustomerAccountDialogComponent implements OnInit {

    customerAccount: CustomerAccount;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private customerAccountService: CustomerAccountService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.customerAccount.id !== undefined) {
            this.subscribeToSaveResponse(
                this.customerAccountService.update(this.customerAccount));
        } else {
            this.subscribeToSaveResponse(
                this.customerAccountService.create(this.customerAccount));
        }
    }

    private subscribeToSaveResponse(result: Observable<CustomerAccount>) {
        result.subscribe((res: CustomerAccount) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CustomerAccount) {
        this.eventManager.broadcast({ name: 'customerAccountListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-customer-account-popup',
    template: ''
})
export class CustomerAccountPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private customerAccountPopupService: CustomerAccountPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.customerAccountPopupService
                    .open(CustomerAccountDialogComponent as Component, params['id']);
            } else {
                this.customerAccountPopupService
                    .open(CustomerAccountDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
