import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ApigatewaySharedModule } from '../../shared';
import {
    ParkingZoneService,
    ParkingZonePopupService,
    ParkingZoneComponent,
    ParkingZoneDetailComponent,
    ParkingZoneDialogComponent,
    ParkingZonePopupComponent,
    ParkingZoneDeletePopupComponent,
    ParkingZoneDeleteDialogComponent,
    parkingZoneRoute,
    parkingZonePopupRoute,
} from './';

const ENTITY_STATES = [
    ...parkingZoneRoute,
    ...parkingZonePopupRoute,
];

@NgModule({
    imports: [
        ApigatewaySharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ParkingZoneComponent,
        ParkingZoneDetailComponent,
        ParkingZoneDialogComponent,
        ParkingZoneDeleteDialogComponent,
        ParkingZonePopupComponent,
        ParkingZoneDeletePopupComponent,
    ],
    entryComponents: [
        ParkingZoneComponent,
        ParkingZoneDialogComponent,
        ParkingZonePopupComponent,
        ParkingZoneDeleteDialogComponent,
        ParkingZoneDeletePopupComponent,
    ],
    providers: [
        ParkingZoneService,
        ParkingZonePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ApigatewayParkingZoneModule {}
