import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ParkingZone } from './parking-zone.model';
import { ParkingZonePopupService } from './parking-zone-popup.service';
import { ParkingZoneService } from './parking-zone.service';

@Component({
    selector: 'jhi-parking-zone-delete-dialog',
    templateUrl: './parking-zone-delete-dialog.component.html'
})
export class ParkingZoneDeleteDialogComponent {

    parkingZone: ParkingZone;

    constructor(
        private parkingZoneService: ParkingZoneService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.parkingZoneService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'parkingZoneListModification',
                content: 'Deleted an parkingZone'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-parking-zone-delete-popup',
    template: ''
})
export class ParkingZoneDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private parkingZonePopupService: ParkingZonePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.parkingZonePopupService
                .open(ParkingZoneDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
