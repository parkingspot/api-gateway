import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ParkingZone } from './parking-zone.model';
import { ParkingZonePopupService } from './parking-zone-popup.service';
import { ParkingZoneService } from './parking-zone.service';

@Component({
    selector: 'jhi-parking-zone-dialog',
    templateUrl: './parking-zone-dialog.component.html'
})
export class ParkingZoneDialogComponent implements OnInit {

    parkingZone: ParkingZone;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private parkingZoneService: ParkingZoneService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.parkingZone.id !== undefined) {
            this.subscribeToSaveResponse(
                this.parkingZoneService.update(this.parkingZone));
        } else {
            this.subscribeToSaveResponse(
                this.parkingZoneService.create(this.parkingZone));
        }
    }

    private subscribeToSaveResponse(result: Observable<ParkingZone>) {
        result.subscribe((res: ParkingZone) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: ParkingZone) {
        this.eventManager.broadcast({ name: 'parkingZoneListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-parking-zone-popup',
    template: ''
})
export class ParkingZonePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private parkingZonePopupService: ParkingZonePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.parkingZonePopupService
                    .open(ParkingZoneDialogComponent as Component, params['id']);
            } else {
                this.parkingZonePopupService
                    .open(ParkingZoneDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
