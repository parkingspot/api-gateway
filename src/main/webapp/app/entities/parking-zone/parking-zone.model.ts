import { BaseEntity } from './../../shared';

export class ParkingZone implements BaseEntity {
    constructor(
        public id?: number,
        public boundaryPolygon?: string,
        public parkingFee?: number,
        public name?: string,
    ) {
    }
}
