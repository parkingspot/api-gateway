import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { ParkingZone } from './parking-zone.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ParkingZoneService {

    private resourceUrl = '/parkingapi/api/parking-zones';

    constructor(private http: Http) { }

    create(parkingZone: ParkingZone): Observable<ParkingZone> {
        const copy = this.convert(parkingZone);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(parkingZone: ParkingZone): Observable<ParkingZone> {
        const copy = this.convert(parkingZone);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<ParkingZone> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to ParkingZone.
     */
    private convertItemFromServer(json: any): ParkingZone {
        const entity: ParkingZone = Object.assign(new ParkingZone(), json);
        return entity;
    }

    /**
     * Convert a ParkingZone to a JSON which can be sent to the server.
     */
    private convert(parkingZone: ParkingZone): ParkingZone {
        const copy: ParkingZone = Object.assign({}, parkingZone);
        return copy;
    }
}
