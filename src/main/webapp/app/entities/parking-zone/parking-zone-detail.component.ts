import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { ParkingZone } from './parking-zone.model';
import { ParkingZoneService } from './parking-zone.service';

@Component({
    selector: 'jhi-parking-zone-detail',
    templateUrl: './parking-zone-detail.component.html'
})
export class ParkingZoneDetailComponent implements OnInit, OnDestroy {

    parkingZone: ParkingZone;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private parkingZoneService: ParkingZoneService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInParkingZones();
    }

    load(id) {
        this.parkingZoneService.find(id).subscribe((parkingZone) => {
            this.parkingZone = parkingZone;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInParkingZones() {
        this.eventSubscriber = this.eventManager.subscribe(
            'parkingZoneListModification',
            (response) => this.load(this.parkingZone.id)
        );
    }
}
