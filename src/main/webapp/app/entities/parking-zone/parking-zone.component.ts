import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { ParkingZone } from './parking-zone.model';
import { ParkingZoneService } from './parking-zone.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-parking-zone',
    templateUrl: './parking-zone.component.html'
})
export class ParkingZoneComponent implements OnInit, OnDestroy {
parkingZones: ParkingZone[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private parkingZoneService: ParkingZoneService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.parkingZoneService.query().subscribe(
            (res: ResponseWrapper) => {
                this.parkingZones = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInParkingZones();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ParkingZone) {
        return item.id;
    }
    registerChangeInParkingZones() {
        this.eventSubscriber = this.eventManager.subscribe('parkingZoneListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
