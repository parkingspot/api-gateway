import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ParkingZoneComponent } from './parking-zone.component';
import { ParkingZoneDetailComponent } from './parking-zone-detail.component';
import { ParkingZonePopupComponent } from './parking-zone-dialog.component';
import { ParkingZoneDeletePopupComponent } from './parking-zone-delete-dialog.component';

export const parkingZoneRoute: Routes = [
    {
        path: 'parking-zone',
        component: ParkingZoneComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParkingZones'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'parking-zone/:id',
        component: ParkingZoneDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParkingZones'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const parkingZonePopupRoute: Routes = [
    {
        path: 'parking-zone-new',
        component: ParkingZonePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParkingZones'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'parking-zone/:id/edit',
        component: ParkingZonePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParkingZones'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'parking-zone/:id/delete',
        component: ParkingZoneDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParkingZones'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
