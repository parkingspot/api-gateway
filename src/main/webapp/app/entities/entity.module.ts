import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ApigatewayCustomerAccountModule } from './customer-account/customer-account.module';
import { ApigatewayParkingZoneModule } from './parking-zone/parking-zone.module';
import { ApigatewayParkingEventModule } from './parking-event/parking-event.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        ApigatewayCustomerAccountModule,
        ApigatewayParkingZoneModule,
        ApigatewayParkingEventModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ApigatewayEntityModule {}
