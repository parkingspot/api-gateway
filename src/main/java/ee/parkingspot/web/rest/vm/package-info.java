/**
 * View Models used by Spring MVC REST controllers.
 */
package ee.parkingspot.web.rest.vm;
